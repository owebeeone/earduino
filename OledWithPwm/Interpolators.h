
#ifndef Include_Interpolators_H__
#define Include_Interpolators_H__

namespace il {

/**
 * Base interpolator class.
 */
class Interpolator {
public:
  virtual float interpolate(float q) const = 0;
  virtual ~Interpolator() = default;
};

class Parameters {

};

class InterpolatorProvider {
public:
  virtual Interpolator* provide(Parameters parameters) = 0;
};

class LinearPositive : public Interpolator {
public:
  float interpolate(float q) const {
    return q;
  }
};

class Inverted : public Interpolator {
public:
  virtual float interpolate(float q) const {
    return 1 - q;
  }
};

class Product : public Interpolator {
public:

  Product(const Interpolator& a, const Interpolator& b) : a(a), b(b) {}

  virtual float interpolate(float q) {
    return a.interpolate(q) * b.interpolate(q);
  }

  const Interpolator& a;
  const Interpolator& b;
};

class ScaleOffset : public Interpolator {
public:

  ScaleOffset(const Interpolator& a,
    const float scale,
    const float offset) : a(a), scale(scale), offset(offset) {}

  virtual float interpolate(float q) {
    return a.interpolate(q) * scale + offset;
  }

  const Interpolator& a;
  const float scale;
  const float offset;
};

class Clamp : public Interpolator {
public:

  Clamp(const Interpolator& a) : a(a) {}

  virtual float interpolate(float q) {
    auto result = a.interpolate(q);
    if (result < 0) {
      return 0;
    }

    if (result > 1) {
      return 1;
    }
    return result;
  }

  const Interpolator& a;
};


class CubicInterpolator : public Interpolator {
public:
  // Interpolates t - t^3 over the section that increases.
  // More specifically this below over [0,1]
  // (((x-0.5)*2/(3^0.5)-((x-0.5)*2/(3^0.5))^3) + (1/(3^0.5)-(1/(3^0.5))^3))
  // /(2 * +(1/(3^0.5)-(1/(3^0.5))^3))
  static constexpr float ROOT3 = 1.73205080756887729352f;
  static constexpr float INV_ROOT3 = 1 / ROOT3;
  static constexpr float OFFSET = (INV_ROOT3 - INV_ROOT3 * INV_ROOT3 * INV_ROOT3);
  static constexpr float AMPLITUDE = 2 * OFFSET;
  float interpolate(float q) {
    float x = (q - 0.5f) * 2 * INV_ROOT3;
    float scalar = (x - x * x * x + OFFSET) / AMPLITUDE;
    return scalar;
  }
};

template <typename F>
class FunctionInterpolator : public Interpolator {
public:

  template<typename... Args>
  FunctionInterpolator(Args... args)
    : function(args...)
  {}

  virtual float interpolate(float q) const {
    return (function.f(q * function.SCALE_X + function.OFFSET_X) - minValue) * invRange;
  }

  const F function;
  const float minValue = function.f(function.MIN_X);
  const float invRange = 1 / (function.f(function.MAX_X) - minValue);
};


class CubicFast {
public:
  static constexpr float MIN_X = -0.5;  // Non scaled or offset.
  static constexpr float MAX_X = 0.5;  // Non scaled or offset.

  static constexpr float OFFSET_X = -0.5;
  static constexpr float SCALE_X = 1.0;

  CubicFast(float a) : a(a) {}

  double f(float x) const {
    return a * x + x * x * x;
  }

  const float a;
};

// Fast cubic interpolator.
using CubicFastInterpolator = FunctionInterpolator<CubicFast>;


class CubicSlowFast {
public:
  static constexpr float MIN_X = 0.0;  // Non scaled or offset.
  static constexpr float MAX_X = 1.0;  // Non scaled or offset.

  static constexpr float OFFSET_X = 0.0;
  static constexpr float SCALE_X = 1.0;

  CubicSlowFast(float a) : a(a) {}

  double f(float x) const {
    return a * x + x * x * x;
  }

  const float a;
};

// Fast cubic interpolator.
using CubicSlowFastInterpolator = FunctionInterpolator<CubicSlowFast>;

class QuarticSlowFast {
public:
  static constexpr float MIN_X = 0.0;  // Non scaled or offset.
  static constexpr float MAX_X = 1.0;  // Non scaled or offset.

  static constexpr float OFFSET_X = 0.0;
  static constexpr float SCALE_X = 1.0;

  QuarticSlowFast(float a, float b, float c) : a(a), b(b), c(c) {}

  double f(float x) const {
    auto x2 = x * x;
    auto x3 = x2 * x;
    auto x4 = x3 * x;
    return a * x + b * x2 + c * x3 + x4;
  }

  const float a;
  const float b;
  const float c;
};

// Fast cubic interpolator.
using QuarticSlowFastInterpolator = FunctionInterpolator<QuarticSlowFast>;

}  // namespace



#endif