#ifndef Include_EspTimerManager_H__
#define Include_EspTimerManager_H__
#include <Arduino.h>

namespace ahl {

// Timer clients implement this base class.
class TimerClient {
  friend class TimerManager;
  friend class TimerClientHead;
public:
  virtual ~TimerClient() {}

  TimerClient() = default;

  TimerClient(TimerClient&& other) {
    next = other.next;
    prev = other.prev;
    if (next != nullptr) {
      next->prev = this;
    }
    if (prev != nullptr) {
      prev->next = this;
    }
  }

  // Returns the next counter trigger value and performs
  // any operations needed at this time for this client.
  virtual uint32 nextIter(uint32 now) = 0;

  // Called by the TimerManager on being added when the 
  // "cycles per second" are known.
  virtual void setCyclesPerUSec(uint32 perUsec) {
    this->perUsec = perUsec;
  }

  TimerClient(const TimerClient&) = delete;
  TimerClient& operator=(const TimerClient&) = delete;

protected:
  uint32 perUsec;
private:
  TimerClient* next = nullptr;
  TimerClient* prev = nullptr;
};

class TimerClientHead : public TimerClient {
public:
  TimerClientHead() {
    next = prev = this;
  }
  uint32 nextIter(uint32 now) {
    return now + 100000000;
  }
};

// Managers a number of TimerClient objects.
class TimerManager {
public:
  TimerManager(uint32 perUsec) : perUsec(perUsec) {
  }

  // Add a timer client. Must not be called from ISR.
  void addClient(TimerClient* client) {
    client->setCyclesPerUSec(perUsec);

    if (client->prev != nullptr || client->next != nullptr) {
      // Already inserted.
      return;
    }

    client->next = clients.next;
    clients.next->prev = client;
    client->prev = const_cast<TimerClientHead*>(&clients);
    clients.next = client; // This must be the last thing done.
  }

  void removeClient(TimerClient* client) {
    if (client->prev == nullptr || client->next == nullptr) {
      // Already removed.
      return;
    }

    auto prev = client->prev;
    client->prev = nullptr;
    auto next = prev->next = client->next;
    next->prev = prev;
    client->next = nullptr;

  }

  // Returns the next time to be called back.
  uint32 performIter(uint32 now) {
    auto nextTime = now - 1;
    auto period = nextTime - now;
    for (TimerClient* client = clients.next; client != &clients; client = client->next) {
      auto result = client->nextIter(now);
      auto resultPeriod = result - now;
      if (resultPeriod < period) {
        nextTime = result;
        period = resultPeriod;
      }
    }
    return nextTime;
  }

private:

  TimerManager(const TimerManager&) = delete;
  TimerManager& operator=(const TimerManager&) = delete;

  TimerClientHead volatile clients;
  const uint32 perUsec;
};

// ESP8266 has a single timer that has a single compare register and 
// is clocked at the CPU clock frequency and has only 32 bit resolution.
class EspTimerSystem {
private:
  static const uint32 CYCLES_PER_SEC = 80000000; // 80 MHz
  static const uint32 CYCLES_PER_uSEC = 80; // 80 Cycles per microseconf

  EspTimerSystem()
    : timerManager(CYCLES_PER_uSEC)
  {
    timer0_isr_init();
  }

public:
  static void init() {
    EspTimerSystem& instance = getInstance();

    timer0_attachInterrupt(isr);
    timer0_write(ESP.getCycleCount() + 1000);
  }

  // Returns singleton instance.
  static EspTimerSystem& getInstance() {
    static EspTimerSystem instance;
    return instance;
  }

  TimerManager& getManager() {
    return timerManager;
  }

  void addClient(TimerClient* client) {
    timerManager.addClient(client);
  }

private:
  
  // The interrupt routine.
  static void isr() {
    EspTimerSystem& instance = getInstance();
    instance.nextIter();
  }

  void nextIter() {

    // We send a time slightly in the future because of the overhead of processing
    // all the clients.  Hence when the the clients are finished processing, we don't
    // need immediately call them again.

    int iterations = 0;
    decltype(ESP.getCycleCount()) next;
    decltype(ESP.getCycleCount()) now;
    do {
      next = timerManager.performIter(ESP.getCycleCount() + THRESHOLD_CYCLES);
      now = ESP.getCycleCount();
      if (now - next < THRESHOLD_CYCLES) {

      }
    } while (++iterations < MAX_ITERATIONS);

    // Next must be not too big or not too small otherwise we never get called back
    // or hog the CPU.
    if (next - now > 1000000) {
      timer0_write(ESP.getCycleCount() + THRESHOLD_CYCLES);
    } else if (next - now < THRESHOLD_CYCLES / 2) {
      timer0_write(ESP.getCycleCount() + THRESHOLD_CYCLES);
    } else {
      timer0_write(next);
    }
  }

  static const unsigned THRESHOLD_CYCLES = 1000;
  static const int MAX_ITERATIONS = 5;
  TimerManager timerManager;
};

}  // namespace

#endif  // Include_EspTimerManager_H__