
#include <Wire.h>
#include "OLED.h"

#include "DigitalOutExtenderBit.h"
#include "EspTimerManager.h"
#include "PwmDriver.h"

bool doPrint = true;
int syncCount = 0;
int setCount = 0;

//WIFI_Kit_8's OLED connection:
//SDA -- GPIO4 -- D2
//SCL -- GPIO5 -- D1
//RST -- GPIO16 -- D0

#define RST_OLED 16
OLED display(4, 5);

// If you bought WIFI Kit 8 before 2017-8-20, you may try this initial
//#define RST_OLED D2
//OLED display(SDA, SCL);

ahl::DigitalOutExtender<6> digitalOutExtender(D6, D8, D7);
ahl::PwmHbridgeDriverExtended pwmExtened1(digitalOutExtender.getBit(4), digitalOutExtender.getBit(5), 1000);
ahl::PwmHbridgeDriverExtended pwmExtened2(digitalOutExtender.getBit(2), digitalOutExtender.getBit(3), 1000);

void testSetup() {
  digitalOutExtender.setup();
  pwmExtened1.setup();
  pwmExtened2.setup();

  ahl::EspTimerSystem::getInstance().addClient(&pwmExtened1);
  ahl::EspTimerSystem::getInstance().addClient(&pwmExtened2);
  ahl::EspTimerSystem::getInstance().init();
}

uint32 lasTime = 0;
int x = 1;

void testLoop() {
  unsigned long now = micros();
  unsigned long sinceLast = now - lasTime;
  using Polarity = ahl::PwmHbridgeDriverBase::Polarity;

  if (sinceLast > 10000) {
    lasTime = now;
    pwmExtened1.setDuty(Polarity::POSITIVE, x / 600.f);
    pwmExtened2.setDuty(Polarity::POSITIVE, (600 - x) / 600.f);

    pwmExtened1.setDuty(Polarity::NEGATIVE, x > 200 ? 0.05f : 0.5f);
    pwmExtened2.setDuty(Polarity::NEGATIVE, x < 400 ? 0.05f : 0.5f);
    x--;
    if (x < 0) {
      x = 600;
    }
  }
}

// --------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------


int delayMillis = 10;
void setup() {

  /*
  pinMode(ledA, OUTPUT);
  pinMode(ledB, OUTPUT);
  digitalWrite(ledA, LOW);
  digitalWrite(ledB, LOW);
  */

  pinMode(RST_OLED, OUTPUT);
  digitalWrite(RST_OLED, LOW);   // turn D2 low to reset OLED
  delay(50);
  digitalWrite(RST_OLED, HIGH);    // while OLED is running, must set D2 in high

  Serial.begin(74880);
  Serial.println("OLED test!");

  // Initialize display
  display.begin();

  // Test message
  display.print("Hello World");
  delay(delayMillis);

  // Test long message
  display.print("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
  delay(delayMillis);

  // Test display clear
  display.clear();
  delay(delayMillis);

  // Test message postioning
  display.print("TOP-LEFT");
  display.print("4th row", 3);
  display.print("RIGHT-BOTTOM", 7, 3);
  delay(delayMillis);

  // Test display OFF
  display.off();
  display.print("3rd row", 2, 8);
  delay(delayMillis);

  // Test display ON
  display.on();
  delay(delayMillis);


  testSetup();
}

int r = 0, c = 0;

unsigned long lastMicros = 0;

void loop() {
  testLoop();

  unsigned long newLastMicros = micros();

  if ((newLastMicros - lastMicros) > 500000) {
    lastMicros = newLastMicros;
  }
  else {
    return;
  }

  if (true) {
    r = r % 4;
    c = micros() % 6;
    if (r == 0) {
      display.clear();
    }

    //digitalWrite(ledA, r & 1 ? HIGH : LOW);
    //digitalWrite(ledB, r & 2 ? HIGH : LOW);
    display.print("Hi Gi", r++, c++);

    if (doPrint) {
      doPrint = false;
      Serial.print("setCount = ");
      Serial.println(setCount);
      Serial.print("syncCount = ");
      Serial.println(syncCount);
    }
  }
}

