
#ifndef Include_LightsManager_H__
#define Include_LightsManager_H__
#include <Arduino.h>

#include <Wire.h>
#include "OLED.h"

#include "DigitalOutExtenderBit.h"
#include "EspTimerManager.h"
#include "PwmDriver.h"
#include "LightsCommandServer.h"

#include "PrivateDetails.private.h"




/** 
 * Manages lighting.
 */
class LightsManager {
public:

  void setup() {
    commandServer.setup();

    pinMode(RST_OLED, OUTPUT);
    digitalWrite(RST_OLED, LOW);  // Turn D0 low to reset OLED
    delay(50);
    digitalWrite(RST_OLED, HIGH);  // While OLED is running, must set D0 in high.
    
    Serial.begin(74880);
    Serial.println("OLED setup stuff!!");

    display.begin();
    display.on();
    display.clear();
    writeDisplay("Lights!", 0);
    delay(1000);

    digitalOutExtender.setup();
    for (unsigned i = 0; i < PWM_BRIDGE_COUNT; ++i) {
      pwmExtened[i].setup();
      ahl::EspTimerSystem::getInstance().addClient(pwmExtened + i);
    }

    ahl::EspTimerSystem::getInstance().init();
  }

  // duty may be > 1 and this will steal duty from the other side of the 
  // H bridge cycle but if the overall duty for shared channels is >2 the
  // weighted average is used.
  void setDuty(unsigned channel, float duty) {
    using Polarity = ahl::PwmHbridgeDriverBase::Polarity;
    Polarity polarity = channel & 1 ? Polarity::NEGATIVE : Polarity::POSITIVE;
    unsigned driverIndex = channel / 2;
    pwmExtened[driverIndex].setDuty(polarity, duty);
  }

  void loop() {
    commandServer.loop();

    lightsLoop();
    displayLoop();
  }

  void lightsLoop() {
    auto nowMillis = millis();
    if (nowMillis - lastMillis > LIGHTS_ITER_TIME_MILLIS) {
      lastMillis = nowMillis;
      iterLights();
    }
  }

  void displayLoop() {
    auto nowMillis = millis();
    if (nowMillis - lastDisplayMillis > DISPLAY_ITER_TIME_MILLIS) {
      lastDisplayMillis = nowMillis;
      iterDisplay();
    }
  }

  void iterDisplay() {
    if (commandServer.isConnected()) {
      writeDisplay(String("W ") + commandServer.getLocalAddress().toString(), 0);
    } else {
      writeDisplay("not connected", 0);
    }
    writeDisplay(String(iteration) + "   ", 1);
  }

  void iterLights() {
    using Polarity = ahl::PwmHbridgeDriverBase::Polarity;
    setDuty(0, iteration / 600.f);
    setDuty(1, (600 - iteration) / 600.f);
    setDuty(2, 2 * (600 - iteration) / 600.f);
    setDuty(3, 2 * (600 - iteration) / 600.f);

    setDuty(4, iteration > 200 ? 0.2f : 1.0f);
    setDuty(5, iteration < 400 ? 0.2f : 1.0f);

    setDuty(6, 1.0f);
    setDuty(7, 1.0f);

    iteration -= 1;
    if (iteration < 0) {
      iteration = 600;
    }
  }

  void writeDisplay(const String& str, uint8_t row, uint8_t col = 0) {
    writeDisplay(str.c_str(), row, col);
  }

  void writeDisplay(const char* str, uint8_t row, uint8_t col = 0) {
    display.print(const_cast<char *>(str), row, col);
  }

private:

  LightsCommandServer commandServer{ PrivateSSID, PrivatePassword, "Central32"};

  static const uint8 RST_OLED = 16;
  OLED display{ 4, 5 };  // Manages the OLED display.
                         // Connects to extended bits via D8, D7 and D6.
  ahl::DigitalOutExtender<8> digitalOutExtender{ D8, D7, D6 };
  static const uint32 PWM_BRIDGE_COUNT = 4;
  static const uint32 PWM_PERIOD_USECS = 2000;
  il::QuarticSlowFastInterpolator ledLinearizer{ 0.5, 0, 1.0 };
  ahl::PwmHbridgeDriverExtended pwmExtened[PWM_BRIDGE_COUNT] = {
    { digitalOutExtender.getBit(0), digitalOutExtender.getBit(1), PWM_PERIOD_USECS, ledLinearizer },
    { digitalOutExtender.getBit(2), digitalOutExtender.getBit(3), PWM_PERIOD_USECS, ledLinearizer },
    { digitalOutExtender.getBit(4), digitalOutExtender.getBit(5), PWM_PERIOD_USECS, ledLinearizer },
    { digitalOutExtender.getBit(6), digitalOutExtender.getBit(7), PWM_PERIOD_USECS, ledLinearizer },
  };

  decltype(millis()) lastMillis = 0;
  static const uint32 LIGHTS_ITER_TIME_MILLIS = 10;
  decltype(millis()) lastDisplayMillis = 0;
  static const uint32 DISPLAY_ITER_TIME_MILLIS = 1000;
  
  int iteration = 0;
};

#endif