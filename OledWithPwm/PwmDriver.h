#ifndef Include_PwmDriver_H__
#define Include_PwmDriver_H__
#include <Arduino.h>
#include "DigitalOutExtenderBit.h"
#include "EspTimerManager.h"
#include "Interpolators.h"

namespace ahl {
 
// Performs "H bridge" PWM driving.
// This is used in the case where two separate outputs require driving from 
// a single H bridge with independent duty cycles (not more than a total of 
// 100%. It allows overdriving of one side of the H bridge (to fully utilizing
// the entire period even when one side is less than 50% duty.
class PwmHbridgeDriverBase : public TimerClient {
public:
  enum class Polarity : uint8 {
    POSITIVE = 0,
    NEGATIVE = 1
  };

  PwmHbridgeDriverBase(unsigned long periodMicros,
    const il::Interpolator& outputTransferFunction)
    : periodMicros(periodMicros), outputTransferFunction(outputTransferFunction)
  {}

  void setDuty(Polarity polarity, float value) {
    value = outputTransferFunction.interpolate(value);
    dutyValue[static_cast<uint8>(polarity)] = value < 0.0f ? 0.0f : value;
    correctPeriods();
  }

  void setCyclesPerUSec(uint32 perUsec) {
    TimerClient::setCyclesPerUSec(perUsec);
    period = perUsec * periodMicros;
    correctPeriods();
  }

  virtual uint32 nextIter(uint32 now) {
    auto sinceLast = now - lastTime;
    auto periodToNextChange = dutyInt[currentDutyArray][currentStep];
    if (sinceLast >= periodToNextChange) {
      lastTime = now;
      sinceLast = 0;
      do {
        ++currentStep;
        if (currentStep >= 3) {
          currentStep = 0;
          currentDutyArray = nextDutyArray;
        }
        periodToNextChange = dutyInt[currentDutyArray][currentStep];
      } while (periodToNextChange <= periodMicros / 2);

      if (currentStep < 2) {
        setOutput(Polarity(currentStep), true);
      } else {
        setOutput(Polarity(), false);
      }
    }
    return lastTime + periodToNextChange;
  }

  virtual void setOutput(Polarity polarity, bool enable) = 0;

private:

  void correctPeriods() {
    auto dutyTotal = dutyValue[static_cast<uint8>(Polarity::POSITIVE)]
      + dutyValue[static_cast<uint8>(Polarity::NEGATIVE)];
    auto adjustmentFactor = 0.5f;

    if (dutyTotal > 2.0f) {
      adjustmentFactor = 1.0f / dutyTotal;
    }

    unsigned long total = 0;
    int nextArray = currentDutyArray <= 0 ? 1 : 0;
    for (auto i = static_cast<uint8>(Polarity::POSITIVE);
      i <= static_cast<uint8>(Polarity::NEGATIVE);
      ++i) {

      total += dutyInt[nextArray][i] = dutyValue[i] * period * adjustmentFactor;
    }
    if (total > period) {
      dutyInt[nextArray][2] = 0;
    } else {
      dutyInt[nextArray][2] = period - total;
    }
    nextDutyArray = nextArray;
  }

  const unsigned long periodMicros;
  const il::Interpolator& outputTransferFunction;
  unsigned long period;
  unsigned long lastTime;
  float dutyValue[2] = { 1.0f, 1.0f };
  volatile unsigned currentDutyArray = 0;
  unsigned currentStep = 0;
  volatile unsigned nextDutyArray = 0;
  unsigned long dutyInt[2][3] = {
      { periodMicros / 2, periodMicros / 2 }, { periodMicros / 2, periodMicros / 2 }};
  bool enableState = false;
  bool polarityState = false;
};

// H bridge driver using regular GPIO pins.
class PwmHbridgeDriver : public PwmHbridgeDriverBase {
public:
  PwmHbridgeDriver(uint8 positivePin, uint8 negativePin, unsigned long periodMicros,
    const il::Interpolator& outputTransferFunction)
    : PwmHbridgeDriverBase(periodMicros, outputTransferFunction), 
      positivePin(positivePin), negativePin(negativePin)
  {}

  void setup() {
    pinMode(positivePin, OUTPUT);
    pinMode(negativePin, OUTPUT);
  }

  virtual void setOutput(Polarity polarity, bool enable) {
    if (enable) {
      bool isPositive = polarity == Polarity::POSITIVE;
      digitalWrite(positivePin, isPositive ? HIGH : LOW);
      digitalWrite(negativePin, !isPositive ? HIGH : LOW);
    }
    else {
      digitalWrite(positivePin, LOW);
      digitalWrite(negativePin, LOW);
    }
  }

  const uint8 positivePin;
  const uint8 negativePin;
};

// H bridge driver using DigitalOutExtenderBit bits.
class PwmHbridgeDriverExtended : public PwmHbridgeDriverBase {
public:
  PwmHbridgeDriverExtended(DigitalOutExtenderBit& positivePin, 
                           DigitalOutExtenderBit& negativePin, 
                           unsigned long periodMicros,
                           const il::Interpolator& outputTransferFunction)
    : PwmHbridgeDriverBase(periodMicros, outputTransferFunction), positivePin(positivePin), negativePin(negativePin)
  {}

  void setup() {
  }

  virtual void setOutput(Polarity polarity, bool enable) {
    if (enable) {
      bool isPositive = polarity == Polarity::POSITIVE;
      positivePin.set(isPositive);
      negativePin.set(!isPositive);
    }
    else {
      positivePin.set(false);
      negativePin.set(false);
    }

    positivePin.syncAll();
  }

  DigitalOutExtenderBit& positivePin;
  DigitalOutExtenderBit& negativePin;
};

}

#endif  // Include_PwmDriver_H__