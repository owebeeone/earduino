
#ifndef Include_DigitalOutExtenderBit_H__
#define Include_DigitalOutExtenderBit_H__
#include <Arduino.h>

namespace ahl {
// Extend digital output signals using a 74HC595, the serial to parallel with latch chip.
// http://www.ti.com/lit/ds/symlink/sn74hc595.pdf
// This requires 3 digital output pins.

// Digital output extender bits implement this interface.
class DigitalOutExtenderBit {
public:
  virtual ~DigitalOutExtenderBit() = default;
  virtual void set(bool value) = 0;
  virtual void syncAll() = 0;

  DigitalOutExtenderBit() = default;
  DigitalOutExtenderBit(const DigitalOutExtenderBit&) = delete;
  DigitalOutExtenderBit& operator=(const DigitalOutExtenderBit&) = delete;
};


// Digital output extender interface.
class DigitalOutExtenderBase {
public:
  virtual ~DigitalOutExtenderBase() = default;
  virtual void set(unsigned bitNo, bool value) = 0;
  virtual void sync() = 0;
  virtual DigitalOutExtenderBit& getBit(unsigned bitNo) = 0;

  DigitalOutExtenderBase() = default;
  DigitalOutExtenderBase(const DigitalOutExtenderBase&) = delete;
  DigitalOutExtenderBase& operator=(const DigitalOutExtenderBase&) = delete;
};

// Implementation of the digital output extender assuming a 74HC595 (or cascaded set of them)
// where only the first "bits" number of them are used.
template <unsigned bits>
class DigitalOutExtender : public DigitalOutExtenderBase {
public:
  static const unsigned numBits = bits;

  DigitalOutExtender(uint8 rclk, uint8 srclk, uint8 ser)
    : rclk(rclk), srclk(srclk), ser(ser)
  {
    for (int i = 0; i < numBits; ++i) {
      bitValues[i].outExtender = this;
    }
  }

  virtual void set(unsigned bitNo, bool value) {
    bitValues[bitNo].set(value);
  }

  virtual void sync() {
    if (!changed) {
      return;
    }
    for (int i = 0; i < numBits; ++i) {
      sendBit(bitValues[i].value);
    }
    latch();
    changed = false;
  }

  virtual DigitalOutExtenderBit& getBit(unsigned bitNo) {
    return bitValues[bitNo];
  }

  virtual void setup() {
    pinMode(rclk, OUTPUT);
    pinMode(ser, OUTPUT);
    pinMode(srclk, OUTPUT);
    digitalWrite(rclk, LOW);
    digitalWrite(srclk, LOW);
  }
private:

  void sendBit(bool value) {
    digitalWrite(ser, value ? HIGH : LOW);
    digitalWrite(srclk, HIGH);
    digitalWrite(srclk, LOW);
  }

  void latch() {
    digitalWrite(rclk, HIGH);
    digitalWrite(rclk, LOW);
  }

  class Bit : public DigitalOutExtenderBit {
  public:
    virtual void set(bool value) {
      if (this->value == value) {
        return;
      }
      this->value = value;
      outExtender->changed = true;
    }

    virtual void syncAll() {
      outExtender->sync();
    }

    DigitalOutExtender* outExtender = nullptr;
    bool value = false;
  };

  const uint8 rclk;
  const uint8 srclk;
  const uint8 ser;
  bool changed = true;
  Bit bitValues[numBits];
};

} // namespace

#endif  // Include_DigitalOutExtenderBit_H__
