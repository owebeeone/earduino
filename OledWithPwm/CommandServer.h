
#ifndef Include_CommandServer_H__
#define Include_CommandServer_H__

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

namespace ahl {

// Basic wifi initializer and server manager.
class CommandServer {
public:

  CommandServer(const char* ssid, const char* password, const char* hostname)
    : server(80), ssid(ssid), password(password), hostname(hostname)
  {}

  virtual void setup() {
    Serial.println("setting up WiFi"); Serial.flush();
    WiFi.persistent(false);
    WiFi.begin(ssid, password);
    Serial.println("WiFi.begin done"); Serial.flush();
    if (!handlersInitialized) {
      handlersInitialized = true;
      Serial.println("initializeHandlers to do"); Serial.flush();
      initializeHandlers();
      Serial.println("initializeHandlers done"); Serial.flush();
    }
  }

  virtual ~CommandServer() = default;

  void loop() {
    connected = WiFi.status() == WL_CONNECTED;
    if (!connected) {
      initialized = false;
      return;
    }

    if (!initialized) {
      initialized = true;
      MDNS.begin(hostname);
      server.begin();
      localAddress = WiFi.localIP();
    }
    server.handleClient();
  }

  bool isConnected() {
    return connected;
  }

  IPAddress getLocalAddress() {
    return localAddress;
  }

protected:
  virtual void initializeHandlers() = 0;

private:
  CommandServer(const CommandServer&) = delete;
  CommandServer& operator=(const CommandServer&) = delete;

  bool connected = false;
  bool initialized = false;
  bool handlersInitialized = false;
  IPAddress localAddress;
  const char* ssid;
  const char* password;
  const char* hostname;
protected:  
  ESP8266WebServer server;
};

}  // namespace


#endif  // Include_CommandServer_H__