


#ifndef Include_LightsCommandServer_H__
#define Include_LightsCommandServer_H__

#include "CommandServer.h"

#define FORM_URI "submit_program"

const char ROOT_TEMPLATE[] = "<!doctype html>\n"
 "<html itemscope=\"\" itemtype=\"http://schema.org/WebPage\" lang=\"en-AU\">\n"
 "  <head>\n"
 "    <title>32 Park Lights</title>\n"
 "  </head>\n"
 "  <body>\n"
 "    <form action=\"/" FORM_URI "\">\n"
 "      <input class=\"gsfi\" id=\"lst-ib\" maxlength=\"2048\" name=\"lp\" autocomplete=\"off\" \n"
 "        title=\"Program\" type=\"text\" value=\"\"\n"
 "        spellcheck=\"false\" >\n"
 "      <input type=\"submit\" value=\"Submit\">\n"
 "    </form>\n"
 "  </body>\n"
 "</html>";

class LightsCommandServer : public ahl::CommandServer {
public:

  LightsCommandServer(const char* ssid, const char* password, const char* hostname)
    : ahl::CommandServer(ssid, password, hostname)
  {}

  virtual void initializeHandlers() {
    server.onNotFound([this]() { onNotFound(); });

    server.on("/", [this]() { onRootHandler(); });
    server.on("/" FORM_URI, [this]() { onSubmitProgram(); });
  }

  void onSubmitProgram() {
    String response = String("");
    server.sendHeader("Location", "/");
    server.send(301, "text/html", response);
  }

  void onRootHandler() {
    String response = String(ROOT_TEMPLATE);
    server.send(200, "text/html", response);
  }

  // Unknown URLs.
  void onNotFound() {
    String message = "File Not Found\n\n";
    message += "URI: ";
    message += server.uri();
    message += "\nMethod: ";
    message += (server.method() == HTTP_GET) ? "GET" : "POST";
    message += "\nArguments: ";
    message += server.args();
    message += "\n";

    for (uint8_t i = 0; i < server.args(); i++) {
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }

    server.send(404, "text/plain", message);
  }

};

#endif  // Include_LightsCommandServer_H__