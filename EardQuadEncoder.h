
#if !defined(__EardQuadEncoder_h__)
#define __EardQuadEncoder_h__

#include "Earduino.h"

namespace quad {

class QuadEncoderBase {
public:
  using State = unsigned char;

  /**
   * Iterate and return the delta position change.
   */
  long iterate() {
    long change = determineChange();
    currentPosition += change;
    return change;
  }

  long getCurrentPosition() {
    return currentPosition;
  }

  virtual State getInput() = 0;

protected:

  long determineChange() {
    // Initialize to the initial values.
    if (lastState == NO_STATE) {
      lastState = getInput();
      return 0;
    }

    State input = getInput();
    if (input == lastState) {
      return 0;
    }

    State index = input | (lastState << 2);
    lastState = input;

    switch (actionTable()[index]) {
      case Action::increment: {
        lastDirection = 1;
        return lastDirection;
      }
      case Action::decrement: {
        lastDirection = -1;
        return lastDirection;
      }
      case Action::unchanged: {
        lastDirection = 0;
        return lastDirection;
      }
      case Action::indeterminate: {
        if (lastDirection) {
          return 2 * lastDirection;
        }
        break;
      }
    }

    return 0;
  }

  enum class Action : unsigned char {
    increment,
    decrement,
    unchanged,
    indeterminate
  };

  static constexpr State inputOf(State index) {
    return index & 0b11;
  }

  static constexpr State lastOf(State index) {
    return (index >> 2) & 0b11;
  }

  static constexpr State swapBits(State bits) {
    return ((bits << 1) & 2) | ((bits >> 1) & 1);
  }

  static constexpr Action decodeAction(State index) {
    return inputOf(index) == lastOf(index) ? Action::unchanged
      : (inputOf(index) ^ lastOf(index)) == 0b11 ? Action::indeterminate
      : (swapBits(inputOf(index)) ^ lastOf(index)) == 0b10 ? Action::increment
      : Action::decrement;
  }

  // The encoder truth table.
  inline static const Action (&actionTable())[16] {
    static const Action table[] = {
      decodeAction(0),
      decodeAction(1),
      decodeAction(2),
      decodeAction(3),
      decodeAction(4),
      decodeAction(5),
      decodeAction(6),
      decodeAction(7),
      decodeAction(8),
      decodeAction(9),
      decodeAction(10),
      decodeAction(11),
      decodeAction(12),
      decodeAction(13),
      decodeAction(14),
      decodeAction(15),
    };

    return table;
  }

  static const State NO_STATE = ~static_cast<State>(0U);
  State lastState = NO_STATE;
  long currentPosition = 0;
  signed lastDirection = 0;
};

/**
 * Creates 2 bits from inputs.
 */
template <eard::Pin pinA, eard::Pin pinB,
    eard::PinMode inputPinMode = eard::PinMode::input, typename Base = QuadEncoderBase>
class QuadEncoder : public Base {
public:

  using State = typename Base::State;

  /**
   * Return the input values as the first 2 bits in the return value.
   */
  virtual State getInput() {
    return (inputA.get() == eard::PinLevel::low ? 0u : 1u)
      | (inputB.get() == eard::PinLevel::low ? 0u : 2u);
  }

private:

  eard::DebounceInput<3, eard::DigitalInputPin<pinA, inputPinMode>> inputA;
  eard::DebounceInput<3, eard::DigitalInputPin<pinB, inputPinMode>> inputB;
};

} // namespace quad

#endif