/**
 * Earduino is a C++ style interface to the Arduino.
 * 
 * The Earduino interface attempts to provide a simple and type safe interface
 * that will catch many compile time errors as well as use of conflicting pins (at run time).
 * 
 * Also, Earduino programs can be compiled on different target hardware.
 */

#if 0
#define UBRRH
#include "Arduino.h"
#include "D:\Program Files (x86)\Arduino\hardware\arduino\avr\variants\eightanaloginputs/pins_arduino.h"
#endif

#ifndef __EARDUINO_H__
#define __EARDUINO_H__

namespace eard {

using Pin = unsigned char;

enum class PinMode : unsigned char {
#if !defined(INPUT)
  input = 0,
  output = 1,
  pullup = 2
#else
  input = INPUT,
  output = OUTPUT,
  pullup = INPUT_PULLUP
#endif 
};

enum class PinLevel : unsigned char {
#if !defined(HIGH)
  low = 0,
  high = 1
#else
  low = LOW,
  high = HIGH
#endif // !HIGH
};

enum class AnalogReferenceMode : unsigned char {
#if !defined(DEFAULT)
  defaultRef = 0,
  internalRef = 2,
  externalRef = 1,
  v1v1Ref = 4,
  v2v56Ref = 5,
#else
  defaultRef = DEFAULT,
  internalRef = INTERNAL,
  externalRef = EXTERNAL,
  // Only define v1v1Ref and v2v56Ref if there is a definition.
  // i.e. cause a compile error if used and not available.
#ifdef INTERNAL1V1
  v1v1Ref = INTERNAL1V1,
#endif
#ifdef INTERNAL2V56
  v2v56Ref = INTERNAL2V56,
#endif
#endif // !DEFAULT

};

class Earduino;
extern Earduino earduino;

/**
  * Define NULL_BASE_PIN to use a null base for pin definitions.
  */
#if !defined(NULL_BASE_PIN)
#define NULL_BASE_PIN false
#endif

  /**
  * Base definitions for all versions of Earduino reside here.
  */
class EarduinoCommon {
public:
  static const bool USE_NULL_BASE_PIN_CLASS = NULL_BASE_PIN;
};

#undef NULL_BASE_PIN

#ifdef Arduino_h

/**
  * Isolation interface for Earduino. (Arduino version).
  */
class Earduino : public EarduinoCommon {
public:
  static const int NUM_PINS = NUM_DIGITAL_PINS;
  static const int NUM_ANALOG_PINS = NUM_ANALOG_INPUTS;
  static const Pin FIRST_ANALOG_PIN = A0;
  static const Pin LED_PIN = LED_BUILTIN;

  Earduino() {}

  static constexpr bool pinHasPWM(Pin pin) {
    return digitalPinHasPWM(pin);
  }

  /** Provides a digital pin number from an analog pin number. */
  static constexpr Pin analogPin(unsigned char pin) {
    return static_cast<Pin>(analogInputToDigitalPin(pin));
  }

  inline void setPinMode(Pin pin, PinMode mode) {
    pinMode(pin, static_cast<uint8_t>(mode));
  }

  inline void setPinLevel(Pin pin, PinLevel pinlevel) {
    digitalWrite(pin, static_cast<uint8_t>(pinlevel));
  }

  inline PinLevel getPinLevel(Pin pin) {
    return static_cast<PinLevel>(digitalRead(pin));
  }

  inline int getAnalog(Pin pin) {
    return analogRead(pin);
  }

  inline void setAnalog(Pin pin, int level) {
    analogWrite(pin, level);
  }

  inline void setAnalogReferenceMode(AnalogReferenceMode mode) {
    analogReference(static_cast<uint8_t>(mode));
  }

  inline void serialBegin(long baud) {
    Serial.begin(baud);
    while (!Serial) {
      ; // wait for serial port to connect. Needed for native USB port only
    }
  }

  inline void delay(int delayTome) {
    ::delay(delayTome);
  }

  inline long millis() {
    return ::millis();
  }

  inline long micros() {
    return ::micros();
  }

  virtual void terminate(const char* message) {
    Serial.println(message);
    Serial.flush();
    exit(1);
  }
};

#else

class TerminateException {
public:
  TerminateException(const char * message)
    : message(message) {}  // TODO Make a copy;

  const char * const message;
};

/**
* Isolation interface for Earduino. (Standalone version.)
*/
class Earduino : public EarduinoCommon {
public:
  static const int NUM_PINS = 20;
  static const int NUM_ANALOG_PINS = 6;
  static const Pin FIRST_ANALOG_PIN = 14;
  static const Pin LED_PIN = 13;

  Earduino() {}

  static constexpr bool pinHasPWM(Pin p) {
    return ((p) == 3 || (p) == 5 || (p) == 6 || (p) == 9 || (p) == 10 || (p) == 11);
  }

  /** Provides a digital pin number from an analog pin number. */
  static constexpr Pin analogPin(unsigned char analogPin) {
    return static_cast<Pin>(((analogPin < 6) ? (analogPin) + FIRST_ANALOG_PIN : -1));
  }

  inline void setPinMode(Pin pin, PinMode mode) {
  }

  inline void setPinLevel(Pin pin, PinLevel pinlevel) {
    checkPin(pin, "pin number out of range.");
    mockPins[pin].digitalLevel = pinlevel;
  }

  inline PinLevel getPinLevel(Pin pin) {
    checkPin(pin, "pin number out of range.");
    return mockPins[pin].digitalLevel;
  }

  inline int getAnalog(Pin pin) {
    if (pin > NUM_PINS) {
      terminate("pin number out of range.");
    }
    return mockPins[pin].analogLevel;
  }

  inline void setAnalog(Pin pin, int level) {
    mockPins[pin].analogLevel = level;
  }

  inline void setAnalogReferenceMode(AnalogReferenceMode mode) {
  }

  inline void serialBegin(long baud) {
  }

  inline void delay(int delayTome) {
  }

  inline long millis() {
    return ++localMillis;
  }

  inline long micros() {
    return ++localMillis * 1000;
  }

  virtual void terminate(const char* message) {
    throw TerminateException(message);
  }

  void setMockPin(Pin pin, PinLevel level) {
    mockPins[pin].digitalLevel = level;
  }

private:
  class MockPin {
  public:
    PinMode digitalMode = PinMode::input;
    PinLevel digitalLevel = PinLevel::low;
    int analogLevel = 0;
  };

  MockPin mockPins[NUM_PINS];

  void checkPin(Pin pin, const char* message) {
    if (pin > NUM_PINS) {
      terminate(message);
    }
  }
  long localMillis = 123;
};

#endif // Arduino_h


template <Pin pin>
class PinAllocationCheck;

/**
 * Allocator class for checking pin allocation.
 */
#if !defined(EARD_DISABLE_PIN_CHECK)
class PinAllocator {
private:
  template <Pin pin>
  friend class PinAllocationCheck;

  void allocatePin(Pin pin) {
    if (pinMap[pin]) {
      earduino.terminate("pin already allocated.");
    }
    pinMap[pin] = true;
  }

  void deAllocatePin(Pin pin) {
    if (!pinMap[pin]) {
      earduino.terminate("deallocating pin that was not allocated.");
    }
    pinMap[pin] = true;
  }

  static PinAllocator& get() {
    static PinAllocator pinAllocator;
    return pinAllocator;
  }

  // Make class non copyable or instantiable.
  PinAllocator() = default;
  PinAllocator(const PinAllocator&) = delete;
  PinAllocator& operator=(const PinAllocator&) = delete;
  unsigned char pinMap[Earduino::NUM_PINS] = {};
};

#else

class PinAllocator {
private:
  template <Pin pin>
  friend class PinAllocationCheck;

  void allocatePin(Pin pin) {
  }

  void deAllocatePin(Pin pin) {
  }

  static PinAllocator& get() {
    static PinAllocator pinAllocator;
    return pinAllocator;
  }
};
#endif

/**
 * Enumerates the input capabilities of Arduino pins.
 */
enum class InCap : unsigned char {
  digital,      /// Input high or low
  analog,       /// analog 
  none          /// No input capability.
};

/**
 * Enumerates the output capabilities of Arduino pins.
 */
enum class OutCap : unsigned char {
  digital,      /// Output high or low
  pwm,          /// PWM output
  analog,       /// DAC output
  none
};

/**
 * Determines the input capability given the pin number.
 */
constexpr InCap inCapOf(Pin pin) {
  return pin >= Earduino::NUM_PINS 
    ? InCap::none  // Pin number is out of range.
    : pin >= Earduino::FIRST_ANALOG_PIN
      ? InCap::analog
      : InCap::digital;
}

/**
 * Determines the input capability given the pin number.
 */
constexpr unsigned char inBits(Pin pin) {
  return pin >= Earduino::NUM_PINS
    ? 0  // Pin number is out of range.
    : pin >= Earduino::FIRST_ANALOG_PIN ? 10 : 1;
}

/**
 * Determines the output capability given the pin number.
 */
constexpr OutCap outCapOf(Pin pin) {
  return pin >= Earduino::NUM_PINS
    ? OutCap::none  // Pin number is out of range.
    : pin >= Earduino::FIRST_ANALOG_PIN
    ? OutCap::digital
    : (Earduino::pinHasPWM(static_cast<unsigned>(pin)) ? OutCap::pwm : OutCap::digital);
}

/**
* Determines the output number of bits.
*/
constexpr unsigned char outBits(Pin pin) {
  return pin >= Earduino::NUM_PINS
    ? 0  // Pin number is out of range.
      : pin >= Earduino::FIRST_ANALOG_PIN ? 1 : (Earduino::pinHasPWM(static_cast<unsigned>(pin)) ? 8 : 1);
}

template <InCap w_inCap, unsigned w_inBits, OutCap w_outCap, unsigned w_outBits>
class PinCapabilities {
public:
  static const InCap inCap = w_inCap;
  static const unsigned inBits = w_inBits;
  static const OutCap outCap = w_outCap;
  static const unsigned outBits = w_outBits;
  static const bool isDigitalInputCapable = inCap == InCap::digital || inCap == InCap::analog;
  static const bool isAnalogInputCapable = inCap == InCap::analog;
  static const bool isDigitalOutputCapable = outCap == OutCap::digital 
    || outCap == OutCap::analog || outCap == OutCap::pwm;
  static const bool isAnalogOutputCapable = outCap == OutCap::pwm || outCap == OutCap::analog;
};

/**
 * Traits for a specific pin. This may be specializable as needed.
 */
template <Pin pin>
class PinTraits : public PinCapabilities<inCapOf(pin), inBits(pin), outCapOf(pin), outBits(pin)> {};

/**
 * Checks that digital i/o pins are not conflicted.
 */
template <Pin pin>
class PinAllocationCheck {
protected:
  PinAllocationCheck() {
    PinAllocator::get().allocatePin(pin);
  }

  ~PinAllocationCheck() {
    PinAllocator::get().deAllocatePin(pin);
  }

  // Don't allow copy of pin classes.
  PinAllocationCheck(const PinAllocationCheck&) = delete;
  PinAllocationCheck& operator=(const PinAllocationCheck&) = delete;
};

/**
 * A base class for pins that can only be accessed by their derived class type.
 * This base type will mean that i/o functions can only be accessed from the
 * derived type. This is the least overhead of any mechanism to access i/o since
 * the optimized version will call Arduino methods directly with immediate load
 * instructions for pin numbers. Also, pin objects themselves will have the 
 * smallest size (no vtable).
 */
template <Pin w_pin>
class NullPinBase {
public:
  NullPinBase(Pin pin) {}
};

template <Pin pin, typename otherBase, bool useNullPinBase = EarduinoCommon::USE_NULL_BASE_PIN_CLASS>
class Selector {
public:
  using Base = NullPinBase<pin>;
};

template <Pin pin, typename otherBase>
class Selector<pin, otherBase, false> {
public:
  using Base = otherBase;
};

/**
 * A base class for output pins using virtual methods. Pins of this type
 * have a generic base class.
 */
class DigitalOutputPinType {
public:
  DigitalOutputPinType(Pin pin) {}
  virtual ~DigitalOutputPinType() {}
  virtual void setLevel(PinLevel pinlevel) = 0;
};

/**
 * A template class for digital output pins.
 */
template <Pin pin, typename PinPbase = typename Selector<pin, DigitalOutputPinType>::Base>
class DigitalOutputPin : public PinPbase, PinAllocationCheck<pin> {
public:
  using Traits = PinTraits<pin>;
  static_assert(Traits::isDigitalOutputCapable, "The pin is not capable of digital output");

  DigitalOutputPin() : PinPbase(pin) {
    earduino.setPinMode(pin, PinMode::output);
  }

  DigitalOutputPin(PinLevel pinlevel) : PinPbase(pin) {
    earduino.setPinMode(pin, PinMode::output);
    set(pinlevel);
  }

  /**
   * Sets this pin to the given PinLevel.
   */
  inline void set(PinLevel pinlevel) {
    earduino.setPinLevel(pin, pinlevel);
  }

  /**
   * May override base class as virtual (depends on base class).
   */
  void setLevel(PinLevel pinlevel) {
    set(pinlevel);
  }
};

/**
* A base class for digital input pins using virtual methods.
*/
class DigitalInputPinType {
public:
  DigitalInputPinType(Pin pin) {}
  virtual ~DigitalInputPinType() {}
  virtual PinLevel getLevel() = 0;
};

/**
 * A template class for digital input pins.
 */
template <Pin pin, 
          PinMode inputPinMode = PinMode::input, 
          typename PinPbase = typename Selector<pin, DigitalInputPinType>::Base>
class DigitalInputPin : public PinPbase, PinAllocationCheck<pin> {
public:
  using Traits = PinTraits<pin>;
  static_assert(Traits::isDigitalInputCapable, "The pin is not capable of digital input");
  static_assert(
    inputPinMode == PinMode::input || inputPinMode == PinMode::pullup, 
    "Input pin must either be input or pullup");

  DigitalInputPin() : PinPbase(pin) {
    earduino.setPinMode(pin, inputPinMode);
  }

  /**
   * Gets the current level of this pin.
   */
  inline PinLevel get() {
    return earduino.getPinLevel(pin);
  }

  /**
   * May override base class as virtual (depends on base class).
   */
  PinLevel getLevel() {
    return get();
  }
};

template <unsigned long debounceTime, typename InputPin>
class DebounceInput : public InputPin {
public:
  inline PinLevel get() {
    eard::PinLevel input = InputPin::get();
    const bool changedNow = input != currentLevel;
    if (isChanged) {
      if (changedNow) {
        long now = eard::earduino.millis();
        long elapsed = now - lastChangeTime;
        if (elapsed > debounceTime) {
          currentLevel = input;
          isChanged = false;
        }
      }
    } else {
      if (changedNow) {
        isChanged = true;
        lastChangeTime = eard::earduino.millis();
      } else {
        isChanged = false;
      }
    }

    return currentLevel;
  }

private:
  long lastChangeTime = eard::earduino.millis();
  PinLevel currentLevel = eard::PinLevel::low;
  bool isChanged = false;
};

/**
* A base class for analog output pins using virtual methods.
*/
class AnalogOutputPinType {
public:
  AnalogOutputPinType(Pin pin) {}
  virtual ~AnalogOutputPinType() {}
  virtual void setLevel(unsigned) = 0;
};

/**
* A template class for analog (and pwm) output pins.
*/
template <Pin pin, typename PinBase = typename Selector<pin, AnalogOutputPinType>::Base>
class AnalogOutputPin : public PinBase, PinAllocationCheck<pin> {
public:
  using Traits = PinTraits<pin>;
  static_assert(Traits::isAnalogOutputCapable, "The pin is not capable of analog output");

  AnalogOutputPin() : PinBase(pin) {
  }

  /**
  * Sets the output level of this pin or PWM duty cycle.
  */
  inline void set(unsigned level) {
    earduino.setAnalog(pin, level);
  }

  /**
  * May override base class (depends on base class).
  */
  void setLevel(unsigned level) {
    set(level);
  }
};

/**
* A base class for analog input pins using virtual methods.
*/
class AnalogInputPinType {
public:
  AnalogInputPinType(Pin pin) {}
  virtual ~AnalogInputPinType() {}
  virtual int getLevel() = 0;
};

/**
* A template class for analog (and pwm) output pins.
*/
template <Pin pin, typename PinBase = typename Selector<pin, AnalogInputPinType>::Base>
class AnalogInputPin : public PinBase, PinAllocationCheck<pin> {
public:
  using Traits = PinTraits<pin>;
  static_assert(Traits::isAnalogInputCapable, "The pin is not capable of analog output");

  AnalogInputPin() : PinBase(pin) {
  }

  /**
  * Gets the current level of this pin.
  */
  inline int get() {
    return earduino.getAnalog(pin);
  }

  /**
  * May override base class (depends on base class).
  */
  int getLevel() {
    return get();
  }
};

/**
 * Fixed point arithmetic numbers handler.
 */
template <typename T, unsigned w_fracBits> 
class FixedPoint {
public:
  static const unsigned FRAC_BITS = w_fracBits;

  static_assert(FRAC_BITS <= sizeof(T) * 8, "Fraction bits too large for given type");

  template <typename U>
  explicit inline FixedPoint(U value, unsigned fracBits = 0)
    : value(value << (FRAC_BITS - fracBits))
  {}

  template <typename U>
  inline FixedPoint operator*(const U& u) {
    return FixedPoint(value * u, FRAC_BITS);
  }

  template <typename U, unsigned w_otherBits>
  inline FixedPoint operator*(const FixedPoint<U, w_otherBits>& u) {
    return FixedPoint(value * u.value, FRAC_BITS + w_otherBits);
  }

  inline operator unsigned long() {
    return static_cast<unsigned long>(value) >> FRAC_BITS;
  }

  inline operator long() {
    return static_cast<long>(value) >> FRAC_BITS;
  }

  inline T floor() {
    return value >> FRAC_BITS;
  }

private:
  T value;
};

} // namespace eard

#define DEFINE_ARDUINO(T) \
eard::Earduino eard::earduino; \
 \
T& get() { \
  static T object; \
  return object; \
} \
 \
void setup(void) \
{ \
  T::staticSetup();\
  get().setup(); \
} \
 \
void loop(void) \
{ \
  get().loop(); \
} \
// DEFINE_ARDUINO

#endif // !__EARDUINO_H__
